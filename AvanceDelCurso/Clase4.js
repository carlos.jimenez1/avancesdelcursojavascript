 var nombre = 'Carlos', edad = 25

/* Funciones sin Parametros
function imprimirNombre(){
  console.log(nombre + ' tiene ' + edad + ' años')
}*/

//Funciones con Parametros
function imprimirNombre(n, e){
console.log(n + ' tiene ' + e + ' años')
}

imprimirNombre(nombre,edad)
imprimirNombre('Juan',18)
imprimirNombre('Enrique',20)
imprimirNombre('Daniel',17)
imprimirNombre(30,'Pedro')
