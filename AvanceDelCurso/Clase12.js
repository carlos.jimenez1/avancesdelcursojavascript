var carlos = {
  nombre: 'carlos',
  edad: 16
}

const MAYORIA_DE_EDAD = 18

/*function Mayor(persona) {
return persona.edad >= MAYORIA_DE_EDAD
}*/

const Mayor = persona => persona.edad >= MAYORIA_DE_EDAD

function imprimirSiEsMayor(persona){
  if(Mayor(persona)){
    console.log(persona.nombre + 'Es mayor de edad')
  }else {
    console.log('Es menor de edad')
  }
}

function Acceso (persona){
  if(!Mayor(persona)){
    console.log('ACCESO DENEGADO')
  }
}
