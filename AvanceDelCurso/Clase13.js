var carlos = {
  nombre: 'carlos',
  peso: 100.1
}

console.log('Al comienzo del año ' + carlos.nombre + ' pesa: ' + carlos.peso.toFixed(1) + ' Kg')

const aunmentarDePeso = persona => persona.peso += 0.2
const adelgazar = persona => persona.peso -= 0.2


for(var i = 1; i <=365; i++){
  var random  = Math.random()
  if(random < 0.25){
    aunmentarDePeso(carlos)
  }else if(random < 0.5){
    adelgazar(carlos)
  }
}

console.log('Al Finalizar del año ' + carlos.nombre + ' pesa: ' + carlos.peso.toFixed(1) + ' Kg')
