var carlos = {
  nombre: 'Carlos',
  apellidos: 'Jimenez Toledo',
  edad: 25
}

function imprimirNombreEnMayusculas(persona){
var { nombre } = persona
console.log(nombre.toUpperCase())
}

imprimirNombreEnMayusculas(carlos)

/*function cumpleaños(persona) {
  persona.edad += 1
}*/

function cumpleaños(persona) {
  persona.edad += 1
  return{
    ...persona,
    edad: persona.edad + 1
  }
}
